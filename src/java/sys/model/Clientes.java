package sys.model;
// Generated 10/01/2019 02:49:01 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Clientes generated by hbm2java
 */
public class Clientes  implements java.io.Serializable {


     private Integer idcliente;
     private Tipoidentificacion tipoidentificacion;
     private String identificacion;
     private String razonsocial;
     private Date fecharegistro;
     private String estado;
     private Set<Facturas> facturases = new HashSet<Facturas>(0);

    public Clientes() {
        tipoidentificacion = new Tipoidentificacion();
    }

    public Clientes(Tipoidentificacion tipoidentificacion, String identificacion, String razonsocial, Date fecharegistro, String estado, Set<Facturas> facturases) {
       this.tipoidentificacion = tipoidentificacion;
       this.identificacion = identificacion;
       this.razonsocial = razonsocial;
       this.fecharegistro = fecharegistro;
       this.estado = estado;
       this.facturases = facturases;
    }
   
    public Integer getIdcliente() {
        return this.idcliente;
    }
    
    public void setIdcliente(Integer idcliente) {
        this.idcliente = idcliente;
    }
    public Tipoidentificacion getTipoidentificacion() {
        return this.tipoidentificacion;
    }
    
    public void setTipoidentificacion(Tipoidentificacion tipoidentificacion) {
        this.tipoidentificacion = tipoidentificacion;
    }
    public String getIdentificacion() {
        return this.identificacion;
    }
    
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
    public String getRazonsocial() {
        return this.razonsocial;
    }
    
    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }
    public Date getFecharegistro() {
        return this.fecharegistro;
    }
    
    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }
    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public Set<Facturas> getFacturases() {
        return this.facturases;
    }
    
    public void setFacturases(Set<Facturas> facturases) {
        this.facturases = facturases;
    }




}


