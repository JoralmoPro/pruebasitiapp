/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys.dao;

import java.util.List;
import sys.model.Clientes;
import sys.model.Tipoidentificacion;

/**
 *
 * @author joralmopro
 */
public interface clienteDao {
    // Lista de empleados
    public List<Clientes> getClientes();
    // Crear empleado
    public boolean nuevoCliente(Clientes cliente);
    // Modificar empleado
    public boolean modificarCliente(Clientes cliente);
    // Eliminar empleado
    public boolean eliminarCliente(Clientes cliente);
    // Select del tipo de documento
    public List<Tipoidentificacion> getTipoIdentificacion();
}
