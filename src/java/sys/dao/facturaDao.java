/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys.dao;

import java.util.List;
import sys.model.Clientes;
import sys.model.Facturadetalle;
import sys.model.Facturas;
import sys.model.Productos;

/**
 *
 * @author joralmopro
 */
public interface facturaDao {
    // Obtener cliente por el documento
    public Clientes getCliente(Integer tipoIdentificacion, String identificacion);
    // Obterner los productos
    public List<Productos> getProductos();
    // Guardar detalle factura
    public void nuevaFacturaDetalle(Facturadetalle facturadetalle);
    // Guardar la factura
    public Facturas nuevaFactura(Facturas factura);
    // Obtener la factura para el pdf
    public Facturas getFactura(Integer idFactura);
}
