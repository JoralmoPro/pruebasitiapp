/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import sys.model.Clientes;
import sys.model.Facturadetalle;
import sys.model.Facturas;
import sys.model.Productos;
import sys.util.HibernateUtil;

/**
 *
 * @author joralmopro
 */
public class facturaDaoImp implements facturaDao{

    @Override
    public Clientes getCliente(Integer tipoIdentificacion, String identificacion) {
        Clientes c = new Clientes();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaccion = session.beginTransaction();
        String hql = "from Clientes as c inner join fetch c.tipoidentificacion where c.identificacion = '"+identificacion+"'  and c.tipoidentificacion = "+tipoIdentificacion;
        
        try{
            c = (Clientes) session.createQuery(hql).uniqueResult();
            transaccion.commit();
            session.close();
        }
        catch (Exception e){
            transaccion.rollback();
            System.err.println(e.getMessage());
        }
        return c;
    }

    @Override
    public List<Productos> getProductos() {
        List<Productos> productos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaccion = session.beginTransaction();
        
        String hql = "from Productos";
        try{
            productos = session.createQuery(hql).list();
            transaccion.commit();
            session.close();
        }
        catch (Exception e){
            transaccion.rollback();
            System.err.println(e.getMessage());
        }
        return productos;
    }

    public void nuevaFacturaDetalle(Facturadetalle facturadetalle) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(facturadetalle);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        }finally{
            if(session!=null){
                session.close();
            }
        }
    }

    public Facturas nuevaFactura(Facturas factura) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(factura);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return factura;
    }

    @Override
    public Facturas getFactura(Integer idFactura) {
        Facturas f = new Facturas();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaccion = session.beginTransaction();
        String hql = "from Facturas as f\n" +
                     "inner join fetch f.clientes\n" +
                     "inner join fetch f.facturadetalles as fd\n" +
                     "inner join fetch fd.productos\n" +
                     "where f.idfactura = "+idFactura;
        
        try{
            f = (Facturas) session.createQuery(hql).uniqueResult();
            transaccion.commit();
            session.close();
        }
        catch (Exception e){
            transaccion.rollback();
            System.err.println(e.getMessage());
        }
        return f;
    }

    
    
}
