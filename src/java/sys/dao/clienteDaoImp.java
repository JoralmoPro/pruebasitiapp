/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys.dao;

import java.util.List;
import sys.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import sys.model.Clientes;
import sys.model.Tipoidentificacion;

/**
 *
 * @author joralmopro
 */
public class clienteDaoImp implements clienteDao{

    @Override
    public List<Clientes> getClientes() {
        List<Clientes> clientes  = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaccion = session.beginTransaction();
        
        String hql = "from Clientes as c inner join fetch c.tipoidentificacion";
        try{
            clientes = session.createQuery(hql).list();
            transaccion.commit();
            session.close();
        }
        catch (Exception e){
            transaccion.rollback();
            System.err.println(e.getMessage());
        }
        return clientes;
    }

    @Override
    public boolean nuevoCliente(Clientes cliente) {
        Session session = null;
        boolean res = false;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(cliente);
            session.getTransaction().commit();
            res = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return res;
    }

    @Override
    public boolean modificarCliente(Clientes cliente) {
        Session session = null;
        boolean res = false;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(cliente);
            session.getTransaction().commit();
            res = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return res;
    }

    @Override
    public boolean eliminarCliente(Clientes cliente) {
        Session session = null;
        boolean res = false;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(cliente);
            session.getTransaction().commit();
            res = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return res;
    }

    @Override
    public List<Tipoidentificacion> getTipoIdentificacion() {
        List<Tipoidentificacion> tipoidentificacion  = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaccion = session.beginTransaction();
        
        String hql = "from Tipoidentificacion";
        try{
            tipoidentificacion = session.createQuery(hql).list();
            transaccion.commit();
            session.close();
        }
        catch (Exception e){
            transaccion.rollback();
            System.err.println(e.getMessage());
        }
        return tipoidentificacion;
    }
}
