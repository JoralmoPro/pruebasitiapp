/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys.bean;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import sys.dao.facturaDao;
import sys.dao.facturaDaoImp;
import sys.model.Clientes;
import sys.model.Facturadetalle;
import sys.model.Facturas;
import sys.model.Productos;

/**
 *
 * @author joralmopro
 */
@ManagedBean
@ViewScoped
public class facturaBean {

    private Clientes cliente = null;
    private Integer idIden;
    private String ident;
    private List<Productos> prodComprar = new ArrayList<Productos>();
    private List<Productos> productos;
    private List<Integer> cantidadesTemp = new ArrayList<Integer>();
    private List<Integer> cantidades = new ArrayList<Integer>();
    private Integer total;
    private Integer idFactura;
    private Facturas factura;

    public facturaBean() {
        cliente = new Clientes();
    }

    /*public Clientes getCliente() {
        return cliente;
    }*/
    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Integer getIdIden() {
        return idIden;
    }

    public void setIdIden(Integer idIden) {
        this.idIden = idIden;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public List<Productos> getProdComprar() {
        return prodComprar;
    }

    public void setProdComprar(List<Productos> prodComprar) {
        this.prodComprar = prodComprar;
    }

    public List<Integer> getCantidadesTemp() {
        return cantidadesTemp;
    }

    public void setCantidadesTemp(List<Integer> cantidades) {
        this.cantidadesTemp = cantidades;
    }

    public List<Integer> getCantidades() {
        return cantidades;
    }

    public void setCantidades(List<Integer> cantidades) {
        this.cantidades = cantidades;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Productos> getProductos() {
        facturaDao fDao = new facturaDaoImp();
        productos = fDao.getProductos();
        productos.forEach((p) -> {
            this.cantidadesTemp.add(0);
        });
        return productos;
    }

    public Clientes getCliente() {
        facturaDao fDao = new facturaDaoImp();
        cliente = fDao.getCliente(this.getIdIden(), this.getIdent());
        return cliente;
    }

    public void agregarProducto(Productos producto, Integer cantidad) {
        System.out.println("--------------------------------------------");
        this.prodComprar.add(producto);
        cantidad = (cantidad == 0) ? 1 : cantidad;
        this.cantidades.add(cantidad);
        this.total();
        System.out.println("--------------------------------------------");
    }

    public void total() {
        Integer suma = 0;
        Integer index = 0;
        for (Productos p : prodComprar) {
            suma += p.getValorunitario() * this.cantidades.get(index++);
        }
        this.setTotal(suma);
    }

    public void total2() {
        Integer suma = 0;
        Integer index = 0;
        for (Facturadetalle fd : this.factura.getFacturadetalles()) {
            suma += fd.getProductos().getValorunitario() * fd.getCantidad();
        }
        this.setTotal(suma);
    }

    public void nuevaFactura() {
        if (this.getCliente() != null && this.getProdComprar().size() > 0) {
            facturaDao fDao = new facturaDaoImp();
            Facturas f = new Facturas(this.cliente, new Date());
            f = fDao.nuevaFactura(f);
            Integer index = 0;
            for (Productos p : prodComprar) {
                Facturadetalle fd = new Facturadetalle(f, p, this.cantidades.get(index++), p.getValorunitario());
                fDao.nuevaFacturaDetalle(fd);
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Bien", "Factura guardada"));
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest origRequest = (HttpServletRequest) context.getExternalContext().getRequest();
            String contextPath = origRequest.getContextPath();
            try {
                FacesContext.getCurrentInstance().getExternalContext()
                        .redirect(contextPath + "/faces/verFactura.xhtml?idFactura="+f.getIdfactura());
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe buscar un usuario primero y agregar productos"));
        }
    }

    public Facturas getFactura() {
        facturaDao fDao = new facturaDaoImp();
        factura = fDao.getFactura(idFactura);
        this.total2();
        return factura;
    }
}
