/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import sys.dao.clienteDao;
import sys.dao.clienteDaoImp;
import sys.model.Clientes;
import sys.model.Tipoidentificacion;

/**
 *
 * @author joralmopro
 */
@ManagedBean
@ViewScoped
public class clienteBean {

    private List<Clientes> clientes;
    private List<SelectItem> tipoidentificacion;
    private Clientes cliente;
    public clienteBean() {
        cliente = new Clientes();
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public List<Clientes> getClientes() {
        clienteDao cDao = new clienteDaoImp();
        clientes = cDao.getClientes();
        return clientes;
    }
    
    public List<SelectItem> getTipoidentificacion(){
        this.tipoidentificacion = new ArrayList<SelectItem>();
        clienteDao cDao = new clienteDaoImp();
        List<Tipoidentificacion> lista = cDao.getTipoIdentificacion();
        tipoidentificacion.clear();
        for(Tipoidentificacion t : lista){
            SelectItem tItem = new SelectItem(t.getIdtipoidentificacion(), t.getDescripcion());
            this.tipoidentificacion.add(tItem);
        }
        return tipoidentificacion;
    }
    
    public void nuevoCliente(){
        clienteDao cDao = new clienteDaoImp();
        boolean res = cDao.nuevoCliente(cliente);
        cliente = new Clientes();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Bien", "Cliente guardado"));
    }
    
    public void modificarCliente(){
        clienteDao cDao = new clienteDaoImp();
        boolean res = cDao.modificarCliente(cliente);
        cliente = new Clientes();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Bien", "Cliente modificar"));
    }
    
    public void eliminarCliente(){
        clienteDao cDao = new clienteDaoImp();
        boolean res = cDao.eliminarCliente(cliente);
        cliente = new Clientes();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Bien", "Cliente eliminado"));
    }
    
}
